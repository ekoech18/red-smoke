package ke.co.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void engPage(View v){
        Intent x = new Intent(this,English.class);
        startActivity(x);
    }

    public void swaPage(View v){
        Intent x=new Intent(this,SWAHILI.class);
        startActivity(x);
    }


}
